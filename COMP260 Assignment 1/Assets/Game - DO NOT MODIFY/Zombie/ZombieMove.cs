﻿using UnityEngine;
using System.Collections;

public class ZombieMove : MonoBehaviour {

	private PlayerControl player;
	private Transform target;
	private NavMeshAgent agent;
	private Rigidbody rigidbody;

	public float maxHealth = 1.0f;
	public float riseHealth = 0.5f;
	public float minHealth = -1.0f;
	public float healRate = 0.1f;
	private float health = 1.0f;

	public bool asleep = true;
	private bool down = false;
	private bool dead = false;

	public float standTorqueP = -2f;	// proportional
	public float standTorqueD = -20f;	// differential
	private float oldAngle = 0;

	public float hitPause = 0;	// in seconds
	private float hitTimeout = 0;

	public float hitDamage = 0.1f;

	public Vector3 centerOfMass = Vector3.zero;
	private Vector3 originalCenterOfMass = Vector3.zero;

	public Vector3 groundCheckOffset;
	public float groundCheckRadius = 1;
	private bool onGround;
	public LayerMask groundLayer;

	public float sightRange = 30.0f;
	public LayerMask sightLayer = Physics.AllLayers;


	void Start () {
		// Find the player object, as target
		player = FindObjectOfType<PlayerControl>();
		target = player.transform;

		// do not allow the NMA direct control over the zombie
		agent = GetComponent<NavMeshAgent>();
		agent.updatePosition = false;
		agent.updateRotation = false;

		// change the centre of mass to near the bottom of the capsule
		// store the old value to reset it on death
		rigidbody = GetComponent<Rigidbody>();
		originalCenterOfMass = rigidbody.centerOfMass;
		rigidbody.centerOfMass = centerOfMass;

		health = maxHealth;
	}

	void FixedUpdate() {		

		if (onGround && !down && !asleep) {
			// move the rigidbody in the direction specified by the nav mesh agent
			// then update the NMA with the agent's actual position
			rigidbody.velocity = agent.velocity;
			agent.nextPosition = rigidbody.position;	

			// add a torque to keep it upright, using a PD controller to damp oscillations
			// BUG: this does not damp circular motion

			float angle = Vector3.Angle(Vector3.up, transform.up);
			float dAngle = angle - oldAngle;
			float torque = angle * standTorqueP + dAngle * standTorqueD;

			Vector3 axis = Vector3.Cross(Vector3.up, transform.up).normalized;
			rigidbody.AddTorque(axis * torque);

			oldAngle = angle;
		}

	}
	
	// NavMeshAgent runs between FixedUpdate and Update

	void Update () {

		CheckOnGround ();

		if (asleep) {
			// wake up if you can see the player
			Vector3 eyes = transform.position + transform.up;
			Vector3 direction = target.position - eyes;
			Ray ray = new Ray(eyes, direction);
			RaycastHit hit;

			if (Physics.Raycast (ray, out hit, sightRange, sightLayer, QueryTriggerInteraction.Ignore)) {
				if (hit.rigidbody != null && hit.rigidbody.transform == target) {
					Wake ();
				}
			}

			if (direction.magnitude < player.Noise) {
				Wake ();
			}
		} else {
			
			if (!dead) {
				// zombies regenerate		
				health = Mathf.Min (1.0f, health + healRate * Time.deltaTime);

				// if they heal enough, they rise again
				if (health > riseHealth) {
					down = false;
				}
			}


			if (!down) {

				if (hitTimeout > 0) {
					hitTimeout -= Time.deltaTime;
				}

				// pause if you're hit, or in the air
				if (!onGround || hitTimeout > 0) {
					agent.Stop ();
				} else {
					agent.Resume ();
				}

				// set the target to be the player's current position
				agent.SetDestination (target.position);			
			}
		}


	}

	void CheckOnGround() {
		// check if a sphere around the bottom of the zombie overlaps with the ground.
		Vector3 centre = transform.TransformPoint(groundCheckOffset);		
		Collider[] colliders = Physics.OverlapSphere(centre, groundCheckRadius, groundLayer, QueryTriggerInteraction.Ignore);
		onGround = colliders.Length > 0;
	}
		
	public void Wake() {
		asleep = false;
	}

	public void Hit(float damage, Vector3 pos, Vector3 force) {

		// wake up
		Wake();

		// pause movement after a hit
		hitTimeout = hitPause;

		// add damage, kill or knock down the zombie if necessary
		if (!dead) {
			health -= damage;
			if (health < 0) {
				down = true;
				rigidbody.centerOfMass = originalCenterOfMass;
			}

			if (health < minHealth) {
				dead = true;	
			}
		}

		// add a push-back force to the zombie
		rigidbody.AddForceAtPosition(force, pos, ForceMode.Impulse);

	}

	public void OnDrawGizmos() {
		// draw the ground check sphere
		Vector3 centre = transform.TransformPoint(groundCheckOffset);		

		if (onGround) {
			Gizmos.color = Color.red;
		}
		else {
			Gizmos.color = Color.yellow;
		}
		Gizmos.DrawWireSphere(centre, groundCheckRadius);

		if (Application.isPlaying && asleep) {
			// wake up if you can see the player
			Vector3 eyes = transform.position + transform.up;
			Ray ray = new Ray(eyes, target.position - eyes);
			RaycastHit hit;

			Vector3 end = ray.origin + ray.direction * sightRange;

			Gizmos.color = Color.red;
			if (Physics.Raycast (ray, out hit, sightRange, sightLayer, QueryTriggerInteraction.Ignore)) {
				end = hit.point;
				if (hit.rigidbody != null && hit.rigidbody.transform == target) {
					Gizmos.color = Color.green;					
				}
			} 
			Gizmos.DrawLine(ray.origin, end);
		}


	}

	public void OnCollisionStay(Collision collision) {

		if (!dead) {
			GameObject other = collision.gameObject;
			PlayerControl who = other.GetComponent<PlayerControl> ();

			if (who != null) {
				// wake up
				Wake();

				// Hurt the player
				who.Hurt (hitDamage * Time.deltaTime);
			}
		}

	}

}
